 {-# LANGUAGE TemplateHaskell, GADTs, ExistentialQuantification, FlexibleInstances #-}

module Actors where

import Data.Hashable
import qualified Data.Map.Lazy as DML
import qualified Data.HashMap.Strict as DHL
import Control.Concurrent.Async
import Graphics.Gloss
import Data.HashMap.Strict
import Data.List
import Data.Ord
import Debug.Trace 
import Control.Lens
import Linear.V2

import Sound
import Items
import Types
import Utility

data Ability b =  Ability {
  _abeffect   :: Effect,
  _abprep     :: Formula Actor SG,
  _abreco     :: Formula Actor SG,
  _abname     :: String,
  _abdesc     :: String,
  _abpic      :: Picture,
  _abtarg     :: Target b
}


data Effect where
  ESeq        :: [Effect] -> Effect                                                               --  to make sure the effects are applied subsequently, use this.
  EConST      :: [Effect ]  -> Effect                                                     --  to make sure that effects are applied at the same time, use this.                                                  --  to make sure that IO effects are played at the same time, use this.
  Delayed     :: Time -> Effect   -> Effect                                                           --  instructs the game to apply the effect in said time :  pushes a free timed action into the queue.
  Damocles    :: (Actor -> Bool)  -> Effect    -> Effect                                              --  instructs the game to only apply the effect if a certain condition is fulfilled ANYTIME after the effect is processed
  EWhen       :: (Actor -> Bool)  -> Effect    -> Effect                                              --  instructs the game to only apply the effect if a certain condition is fulfilled WHEN the effect is processed. 
  OtherModif  :: (Formula Actor PT) -> [STAT] -> (Float -> Float -> Float) -> Effect            --  modify specified target's stat
  SelfModif   :: (Formula Actor SG) -> [STAT] -> (Float -> Float -> Float) -> Effect            --  modify caster's stat
  SelfModifWO :: (Formula Actor PT) -> [STAT] -> (Float -> Float -> Float) -> Effect            -- modify caster's stat with target's
  Amplified   :: Float -> Effect                                                                     --  amplifies effects by a factor
  Afflict     :: AfflictState -> Affliction -> Effect                                                 --  add said affliction
  Special     :: (Actor -> Actor ) -> Effect                                                          --  special effects
  MoveEff     :: (Coord -> Coord)  -> Effect                                                          --  moves actor
  MovingArea  :: [Direction]  -> Effect                                                              --  instructs game to apply effect in the specified relative moving area
  Area        :: [Coord]      -> Effect                                                              --  instructs game to apply effect in the specified relative area
  EIO         :: (Source -> IO ())  -> Effect                                                     --  launches the IO action when the effect is processed.
  SfxPlay     :: SoundEffect        -> Effect                                                     --  plays the given sound effect when the effect is processed.


data Source       = Self 
                  | Other 
                  | OtherItem 
                  | OtherDefined ActorId 
                  | Unsourced  deriving (Show,Eq,Read, Ord)

data Prepared     = SINGLEC
                  | SELF
                  | MULTIC Int -- maximum number of targets
                  | SPEC deriving (Show,Eq,Read,Ord)

data Finished     = SC Coord
                  | SF
                  | MC [Coord]
                  | SP deriving (Show,Eq,Read,Ord)

data Range        = TRANS [Coord] 
                  | BLOCK [Coord] 
                  | SELFR 
                  | ANYR 
                  | FIXED [Coord]
                  | NONR deriving (Show,Eq,Read,Ord)

data Target a where
  CONFIRM :: Finished -> Target Finished
  PREPARE :: Range    -> Prepared -> Target Prepared

instance Show (Target a) where
  show (CONFIRM fn     )  =  "Confirmed target :" ++ show fn
  show (PREPARE rng prp)  =  "Target Model : "++ show rng ++ " - " ++ show prp 



data Actor = Actor{
    _aname      :: String,
    _acoord     :: V2 Int,
    _abilities  :: [Ability Prepared],
    _astatus    :: ActorStatus,
    _inventory  :: DML.Map Coord Item,
    _aclass     :: ActorClass
} 

data ActorStatus = ActorStatus{
    _spiStats   :: SpiStats,
    _bodyStats  :: BodyStats,
    _actState   :: ActionState,
    _funcStats  :: FuncStats,
    _afflict    :: DML.Map AfflictState Affliction
}


type Priority = Float
type Affliction = (Priority, (Float -> Actor -> Actor))
data AfflictState =  Timer Float | AFlag Bool | Stage Int Float deriving (Show,Eq,Ord,Read)

instance Hashable AfflictState where
  hashWithSalt s x = case x of
      Timer x   -> s + hash x
      AFlag b   -> s + hash b
      Stage n x -> s + hash (fromIntegral n :: Float) + hash x



data FuncStats = FuncStats {
  _energy         :: Float,
  _concentration  :: Float,
  _health         :: Float 
} deriving (Show,Eq,Read)

data BodyStats  = BodyStats {   
  _constitution :: Float,
  _strength     :: Float,
  _tolerance    :: Float,
  _agility      :: Float,
  _precision    :: Float
} deriving (Show,Eq,Read)

data SpiStats = SpiStats { 
  _power        :: Float, 
  _passion      :: Float, 
  _grounding    :: Float, 
  _awareness    :: Float, 
  _insight      :: Float, 
  _resilience   :: Float, 
  _luck         :: Float, 
  _despair      :: Float
} deriving (Show,Eq,Read)


data ActionState = Ready | Recovering Float Source | InAction Float | Inactive deriving (Show,Eq,Read) -- number of game ticks missing to go back to Ready, Source : Recovering from own ability ? Recovering from external ability ? (soul of the attacker) Recovering from something else ? (exhaustion; bulu spell)

data ActorClass = ActS Sentient | ActI Immobile deriving (Show,Ord,Eq,Read)

data Sentient = Hero | Fencer | Archer deriving (Show,Ord,Eq,Read)

data Immobile = Statue Int | Altar Int deriving (Show,Ord,Eq,Read)


zeroFunc = FuncStats{
  _health = 1,
  _energy = 1,
  _concentration = 1
}

zeroActor = Actor {
    _abilities  = [],
    _aname      = "",
    _acoord     = V2 0 0,
    _astatus    = zeroStatus,
    _inventory  = DML.fromList [],
    _aclass     = ActI (Altar 777)
}

zeroStatus = ActorStatus {
    _spiStats   = zeroStats,
    _bodyStats  = zeroBody,
    _actState   = Ready,
    _funcStats  = zeroFunc,
    _afflict    = DML.fromList []
}

zeroStats = SpiStats {
    _power        = 0, 
    _passion      = 0, 
    _grounding    = 0, 
    _awareness    = 0, 
    _insight      = 0, 
    _resilience   = 0, 
    _luck         = 0, 
    _despair      = 0
}

zeroBody = BodyStats {
  _constitution = 1,
  _strength     = 1,
  _tolerance    = 1,
  _agility      = 1,
  _precision    = 1
 }

data STAT = POW 
 | PAS
 | GRN
 | AWA
 | INS
 | RES
 | LCK
 | DES
 | CON
 | STR
 | TOL
 | PRE
 | AGI
 | CC
 | HP
 | EN deriving (Show,Eq,Ord,Read)

makeLenses ''Actor
makeLenses ''Ability
makeLenses ''ActorStatus
makeLenses ''SpiStats
makeLenses ''BodyStats
makeLenses ''FuncStats

placeSingleActor :: ActorId -> Coord -> DHL.HashMap ActorId Actor -> DHL.HashMap ActorId Actor
placeSingleActor aID crd actors = DHL.adjust (placeActor crd) aID actors

placeActor ::  Coord -> Actor -> Actor
placeActor crd actor = actor & acoord .~ crd

displaceSingleActor :: ActorId -> Coord -> DHL.HashMap ActorId Actor -> DHL.HashMap ActorId Actor
displaceSingleActor aID crd actors = DHL.adjust (displaceActor crd) aID actors

displaceActor :: Coord -> Actor -> Actor
displaceActor crd actor = actor & acoord %~ (+crd)


--UTILITY


getID :: ActorId -> Identifier
getID (AID _ idn) = idn

getSoul :: ActorId -> Soul
getSoul (AID sl _) = sl

getBySoul :: Int -> DHL.HashMap ActorId Actor -> [(ActorId,Actor)]
getBySoul n actors = Prelude.filter ((<~> (AID (SL n 0) 0) ).getFirst) $ DHL.toList actors

isReady :: Actor -> Bool
isReady a = a^.astatus.actState == Ready
actorInfo :: Actor -> [String]
actorInfo act = [ 
  "Name    : " ++ act^.aname,
  "Class   : " ++ (show $ act^.aclass),
  "Status  : " ++ (show $ act^.astatus.actState), 
  "HP      : " ++ (show $ act^.astatus.funcStats.health),
  ".."]

getPropFromId ::  DHL.HashMap ActorId Actor -> (Actor -> a) -> ActorId -> a
getPropFromId acts f aid = f $ acts ! aid
  
--STATS
gFun  act = act^.astatus.funcStats
gBod  act = act^.astatus.bodyStats
gSpi  act = act^.astatus.spiStats

instance Timeable ActionState where
  getTime (InAction t)  = trace ("this actor is in action.. time remaining : " ++ (show t)) $ t
  getTime Inactive  = (1/0)
  getTime Ready = 0
  getTime (Recovering x _) = x
  passTime _ Inactive = Inactive
  passTime t (InAction t2)
    | t-t2 <= 0 = Ready
    | otherwise = InAction (t2-t)
  passTime t Ready = Ready -- watch out for this
  passTime t2 (Recovering t b)
    | t-t2 <= 0 = Ready
    | otherwise = Recovering (t-t2) b
  passTimeIO t any'= return $ passTime t any'

instance Timeable Actor where
  getTime act     = getTime $ act^.astatus.actState
  passTime t act  = passActorTime t act
  passTimeIO t act = return $ passTime t act

instance Timeable AfflictState where
  getTime (Timer t)   = t
  getTime (Stage n t) = t
  getTime _           = (1/0) -- so that flag afflictions don't get cleaned up with time              
  passTime t2 (Timer t)   = Timer $ t-t2
  passTime t2 (Stage n t) = Stage n (t-t2)
  passTime t2 x           = x
  passTimeIO t2 x         = return $ passTime t2 x


passActorTime :: Float -> Actor -> Actor          
passActorTime t act = cleanState $ procAfflict $ passState t $ passAfflictions t act
  where
    afflictions = getSeconds $ sortBy (comparing fst) $ [(p,aff t) | (p,aff) <- getSeconds $ DML.toList $ act^.astatus.afflict] --a bit long : get the afflictions with the float argument supplied by the time elapsed; order them by their priority; and remove the priorities.
    procAfflict act = Data.List.foldr (.) id afflictions act -- make the actor suffer every affliction
    passState t act  = act & astatus.actState %~ passTime t
    passAfflictions t act = act & astatus.afflict %~ DML.mapKeys (passTime t)
    cleanState act        = act & astatus.afflict %~ DML.filterWithKey (curry $ (>0).getTime.fst) -- only retain the afflictions that are still active.

moveActor :: Coord ->Actor -> Actor
moveActor crd act = act & acoord %~ (+crd)
                        & astatus.actState .~ Ready
moveActor' :: [Coord] ->Actor -> Actor
moveActor' crds act = trace " actormoved" $ act   & acoord %~ (+ (sum crds))
                                                  & astatus.actState .~ Ready


getMoveArea :: Coord 
            -> Int --number of steps already taken 
            -> Int --maxstride
            -> [Coord]
getMoveArea (V2 px py) n maxstride  = let sz = (maxstride-n)  in [ V2 (px+x) (py+y) | x <- [-sz..sz],y <-[-sz..sz],(-x-y)<=sz,x+y<=sz,x-y<=sz,y-x <=sz]


getMaxStride :: Actor -> Int
getMaxStride act = maxstride 
  where
    maxstride =truncate $ 0.5 * (energ**2)*(con+agi) + energ*pas + (0.5*lck)
    energ = (gFun act)^.energy
    con   = (gBod act)^.constitution
    agi   = (gBod act)^.agility
    pas   = (gSpi act)^.passion
    lck   = (gSpi act)^.luck

getStrideTime :: Actor -> Float
getStrideTime act = time
  where
    bodyQuot = (energ**2) * (agi*2+str)
    spiQuot  = (energ)    * (des*2+pow)
    totalQuot = (bodyQuot+spiQuot / 30) + (lck/100) 
    time  = 100 / totalQuot
    energ = (gFun act)^.energy
    str   = (gBod act)^.strength
    agi   = (gBod act)^.agility
    des   = (gSpi act)^.despair
    pow   = (gSpi act)^.power
    lck   = (gSpi act)^.luck

    
isActorActive:: Actor -> Bool
isActorActive act = case act^.astatus.actState of
  Inactive  -> False
  _         -> True 

isPlayer' :: (ActorId -> Actor -> Bool)
isPlayer' k v = (k <~> playerSoul)

prepAction :: Float -> Actor -> Actor
prepAction time act = act & astatus.actState .~ InAction (time + 0.0001) -- the action delta. to make sure the action is executed before the actor is called again.

-------------- ABILITY STUFF

hardNumberS :: Float -> Formula Actor SG
hardNumberS x =  Raw x
 
hardNumberP :: Float -> Formula Actor PT
hardNumberP x = Raw x

gSt ::  STAT -> Actor -> Float
gSt st act = case st of 
  POW   ->(gSpi act)^.power
  PAS   ->(gSpi act)^.passion
  GRN   ->(gSpi act)^.grounding
  AWA   ->(gSpi act)^.awareness
  INS   ->(gSpi act)^.insight
  RES   ->(gSpi act)^.resilience
  LCK   ->(gSpi act)^.luck
  DES   ->(gSpi act)^.despair
  CON   ->(gBod act)^.constitution
  STR   ->(gBod act)^.strength
  TOL   ->(gBod act)^.tolerance
  PRE   ->(gBod act)^.precision
  AGI   ->(gBod act)^.agility
  HP    ->(gFun act)^.health
  EN    ->(gFun act)^.energy
  CC    ->(gFun act)^.concentration
  _     -> 0   

setSt st = case st of 
  POW   ->astatus.spiStats. power
  PAS   ->astatus.spiStats. passion
  GRN   ->astatus.spiStats. grounding
  AWA   ->astatus.spiStats. awareness
  INS   ->astatus.spiStats. insight
  RES   ->astatus.spiStats. resilience
  LCK   ->astatus.spiStats. luck
  DES   ->astatus.spiStats. despair
  CON   ->astatus.bodyStats. constitution
  STR   ->astatus.bodyStats. strength
  TOL   ->astatus.bodyStats. tolerance
  PRE   ->astatus.bodyStats. precision
  AGI   ->astatus.bodyStats. agility
  HP    ->astatus.funcStats. health
  EN    ->astatus.funcStats. energy
  CC    ->astatus.funcStats. concentration
getPic :: Ability a -> Picture
getPic ab = ab^.abpic

getNbab :: Actor -> Int
getNbab ab = length $ ab^.abilities


finish :: [Coord] -> Ability Prepared -> Ability Finished
finish crds ab = ab & abtarg .~ newtarg
  where
    newtarg = case ab^.abtarg of
      PREPARE _ SPEC                    -> CONFIRM SP
      PREPARE NONR SELF                 -> CONFIRM SF
      PREPARE (FIXED crds') SINGLEC     -> CONFIRM $ SC $ head crds'
      PREPARE (FIXED crds') (MULTIC n)  -> CONFIRM $ MC $ take n crds'
      PREPARE _ SINGLEC                 -> CONFIRM $ SC $ head crds
      PREPARE _ (MULTIC n)              -> CONFIRM $ MC $ take n crds
        


instance Show (Ability Prepared) where
  show ab = "Ability in preparation. \n"++ 
            "Name : " ++ ab^.abname ++ "\n" ++
            "Description : " ++ ab^.abdesc  ++ "\n" ++
            (show $ ab^.abtarg)
      
instance Show (Ability Finished) where
  show ab = "Finished ability awaiting execution. \n"++ 
            "Name : " ++ ab^.abname ++ "\n" ++
            "Description : " ++ ab^.abdesc  ++ "\n" ++
            (show $ ab^.abtarg)
            
