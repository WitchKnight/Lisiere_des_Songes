{-# LANGUAGE TemplateHaskell, GADTs, ExistentialQuantification #-}
module GameLogic where

import Control.Lens
import Control.Concurrent
import Control.Concurrent.Async
import Control.Monad
import Data.Dequeue
import Data.HashSet as DH
import Data.List(partition,sortBy)
import Data.Ord
import Linear.V2
import Linear.V3
import Graphics.Gloss
import Data.HashMap.Strict as DHL
import Debug.Trace
import System.Exit
import Sound.ALUT


import Battle
import Items
import Actors
import ActorGen
import Level
import LevelGen
import Types
import Utility
import Random
import Game
import Sound



updateGame :: Float -> Game -> IO Game
updateGame dt game = newgame
  where 
    newgame = do
      ng1 <- normalUpdate game
      ng2 <- turnUpdate ng1
      return ng2
    normalUpdate game = do 
      -- handle Snake --  
      -- handle Music --
      mgame       <-  handleMusic game
      scgame      <-  updateVisualScene dt mgame    -- needs to be IO because of the console
      flgame      <-  updateFlags dt scgame         -- needs to be IO for the write to disk, read from disk, and quit actions. writes to console.
      return flgame
    turnUpdate g =  case game ^.scene of
      Level gL NoMenu -> case gL^.turnStage of
        BaseTurn                  -> do 
                                      putStrLn "first stage of turn"
                                      prepareTurn g -- IN THIS STAGE; TIME IS PASSED. CRITICAL STAGE.
        ActorsReady _             -> planTurns g -- IO for concurrent planning
        PlayerReady [] actions _  -> return $ g & scene %~ setTurn ( PlansDone actions) -- when there are no more player characters left; advance stage
        PlayerReady _ _ _         -> return g -- Input takes the work in this stage.
        PlansDone _               -> plantActions g -- IO can happen during the processing of the actions.
        TurnDone                  -> return $ g & scene %~ resetTurn
        _                         -> return g
      _         -> return g      --of course turns only apply when the game is in a level.
  

prepareTurn :: Game -> IO Game
prepareTurn game = do
  let Level gL _  = game^.scene
  let  t1           = DHL.foldr min (1/0) $ DHL.map getTime $ DHL.filter isActorActive $ gL^.actors
  let t = if t1 >0 then t1 else 10
  newgame <- passTimeIO t game
  let Level ngL _   = newgame^.scene
  let actorIds      = keys $ DHL.filter isReady $ getActors newgame
  let newlevel      = ngL & turnStage .~ ActorsReady actorIds
  let newscene      = Level newlevel NoMenu
  pure $ newgame & scene .~ newscene
    
    
    
    
   
    

planTurns :: Game -> IO Game
planTurns game = do 
    let Level gL _  = game^.scene
    let ActorsReady allIds = gL^.turnStage
    let (plactorIds,npactorIds)  =  partition (<~> playerSoul) allIds
    let planNPCTurn actId = let act = ((gL^.actors) ! actId) in case act^.aclass of   ActI _  -> putStrLn (act^.aname ++ " : immobile actor, no action...") >> return (ACT NAction actId)
                                                                                      _       -> putStrLn (act^.aname ++ " : default action : no action"   )>> return (ACT NAction actId)
    plannedTurns <- mapConcurrently planNPCTurn npactorIds                                                                        
    let newlevel  = gL & turnStage .~ PlayerReady plactorIds plannedTurns Start
    let newscene  = Level newlevel NoMenu
    return $ game & scene .~ newscene


plantActions :: Game -> IO Game
plantActions g  = do
  let Level gL _  = g^.scene
  let PlansDone actionsToPlant = gL^.turnStage 
  -- actionqueue format with the time
  let newactions  = [(a,howLong a gL) | a <- actionsToPlant]
  -- prepare the actors to be updated with the InAction state
  let newActorWave = [(aid, prepAction time) | (ACT _ aid,time) <- newactions] 
  let ngL = gL & actionqueue  %~ (sortBy (comparing snd)).(++ newactions)
               & turnStage    .~ TurnDone
               & actors       %~ adjustMultiple' newActorWave 
  pure $ g & scene .~ Level ngL NoMenu

howLong :: AAction -> GameLevel -> Float
howLong (ACT x aid) gL = case x of
  MAction _   -> getPropFromId (gL^.actors) (getStrideTime) aid 
  ABAction ab -> let act = (gL^.actors) ! aid in eval act act $ ab^.abprep
  _           -> 0


confirmPlayerTurn :: GameLevel -> TurnStage -> TurnStage
--if you confirm the turn without having done anything; returns NAction
confirmPlayerTurn gL (PlayerReady (x:xs) acts Start) = PlayerReady xs ((ACT NAction x):acts) Start
--if you confirm the turn with the player having moved, only returns ok if the player moved within his capacity
confirmPlayerTurn gL (PlayerReady (x:xs) acts (Move l max') ) =  if length l <= max' then trace "move confirmed" $
 PlayerReady xs ((ACT (MAction l) x):acts) Start else trace "can't move there" $ (PlayerReady (x:xs) acts (Move l max') ) 

confirmPlayerTurn gL (PlayerReady (x:xs) acts (ChooseAbility n _)) = newStatus
  where
    act = (gL^.actors) ! x
    selab = (act^.abilities) !! n
    PREPARE range targetType = selab^.abtarg
    -- if there is no range; ie, that there is nothing to select, the game directlies pushes the ability into the event queue and moves to the next planning actor
    -- otherwise we just move to target selection.
    newStatus = let newact = ABAction (finish [] selab) in case range of 
      NONR    -> PlayerReady xs ((ACT newact x):acts) Start
      FIXED _ -> PlayerReady xs ((ACT newact x):acts) Start
      anyelse -> trace "Moving to target selection ..." $ PlayerReady (x:xs) acts $ ChooseTarg (PREPARE range targetType) [] (V2 0 0) n
-- if you confirm the turn with the player having chosen its target...
confirmPlayerTurn gL (PlayerReady (x:xs) acts (ChooseTarg (PREPARE tg SINGLEC) crds pnt abn )) = 
  let ab = getPropFromId (gL^.actors) ( (!! abn).(^.abilities) ) x 
      pcrd = getPropFromId (gL^.actors) ( ^.acoord) x 
      in case crds of
    c:[]  ->  PlayerReady xs ( (ACT (ABAction (finish [pcrd+c] ab)) x) :acts) Start 
    _     ->  PlayerReady (x:xs) acts (ChooseTarg (PREPARE tg SINGLEC) crds pnt abn )


updateVisualScene :: Float -> Game -> IO Game  
updateVisualScene dt game = case game^.scene of
  Level g _    -> return $ game & camera %~ (+ addveccam)
    where
      newveccam = game^.cameraV
      addveccam :: V3 Float
      addveccam = newveccam * 2.0
  _         -> return game
      

handleMusic :: Game -> IO Game
handleMusic game = do
  let SndSt mPlaying musTheme = game^.soundState
  mgame <- if (not $ mPlaying || silentZone game) then do
    putMVar musicAllow ()
    forkIO $ playMusic musTheme
    let mgame = game & soundState .~ SndSt True musTheme
    return mgame 
  else return game
  return mgame

updateFlags :: Float -> Game -> IO Game
updateFlags dt game
  | flags ! QUIT  = exitSuccess
  | otherwise     = return game
  where
    flags = game^.spFlags

enterMove :: Game -> TurnStage -> TurnStage
enterMove game = \tSt -> case tSt of 
  (PlayerReady (a:as) actions _ ) -> PlayerReady (a:as) actions $ Move [] $ getPropFromId (getActors game) (getMaxStride) a 
  otherState                 -> otherState 


enterAbility :: Game -> TurnStage -> TurnStage
enterAbility game = \tSt -> case tSt of 
  (PlayerReady (a:as) actions _ ) -> PlayerReady (a:as) actions $ ChooseAbility 0 $ getPropFromId (getActors game) (getNbab) a 
  otherState                 -> otherState 

goPlayer  :: Int -> TurnStage -> TurnStage 
goPlayer n (PlayerReady a b (Move l m)) = PlayerReady a b (Move (n:l) m)
goPlayer _ other = other

goTarget' :: Int -> TurnStage -> TurnStage 
-- if it's 
goTarget' n (PlayerReady a b (ChooseTarg (PREPARE (FIXED crds') tp) crds pnt abn))
  | newMv `elem` crds' = PlayerReady a b (ChooseTarg (PREPARE (FIXED crds') tp) crds (getMv n + pnt) abn)
  | otherwise = (PlayerReady a b (ChooseTarg (PREPARE (FIXED crds') tp) crds pnt abn))
  where newMv = pnt + getMv n
goTarget' n (PlayerReady a b (ChooseTarg (PREPARE (BLOCK crds') tp) crds pnt abn)) = 
  let newMv = pnt + getMv n in 
    if newMv `elem` crds'   then  PlayerReady a b (ChooseTarg (PREPARE (BLOCK crds') tp) crds (getMv n + pnt) abn)
                            else  trace "can't target out of bounds" $ PlayerReady a b (ChooseTarg (PREPARE (BLOCK crds') tp) crds pnt abn)
goTarget' n (PlayerReady a b (ChooseTarg (PREPARE (TRANS crds') tp) crds pnt abn)) = 
  let newMv = pnt + getMv n in 
    if newMv `elem` crds'   then  PlayerReady a b (ChooseTarg (PREPARE (TRANS crds') tp) crds (getMv n + pnt) abn)
                            else  PlayerReady a b (ChooseTarg (PREPARE (TRANS crds') tp) crds pnt abn)
goTarget' _   other = other

goBack    :: TurnStage -> TurnStage 
goBack (PlayerReady a b (Move [] _))          = PlayerReady a b Start
goBack (PlayerReady a b (ChooseAbility _ _))  = PlayerReady a b Start
goBack (PlayerReady a b (Move  (_:ol) m))     = PlayerReady a b (Move ol m)
goBack (PlayerReady a b (ChooseTarg _ _ _ _)) = PlayerReady a b Start
goBack other = other



goAbility  :: Int -> TurnStage -> TurnStage 
goAbility n (PlayerReady a b (ChooseAbility x max'))  = PlayerReady a b (ChooseAbility m max')
  where
    m
      | newPos >= 0 && newPos < max' = newPos
      | otherwise = x
    newPos = (fromCoord .(+ (getMv n)). toCoord) x
    toCoord = \x -> V2 x 0
    fromCoord = \(V2 x y) -> x
goAbility _ other = other

addTarget :: TurnStage -> TurnStage 
addTarget (PlayerReady a b (ChooseTarg tg crds pnt abn)) =(PlayerReady a b (ChooseTarg tg  ncrds pnt abn))
    where
      (PREPARE _ ttyp) = tg
      ncrds
        | pnt `elem` crds = crds
        | otherwise = case ttyp of
          MULTIC _ -> (pnt:crds)
          SINGLEC -> [pnt]
          _      -> crds

