 {-# LANGUAGE TemplateHaskell, GADTs, FlexibleInstances  #-}
module Level where

import Control.Monad
import Data.List
import Debug.Trace
import Data.Ord
import Control.Lens
import qualified Data.HashMap.Strict as DHL
import qualified Data.Map.Strict as M
import Linear.V2

import Data.HashMap.Strict hiding (map,filter)
import Console
import Actors
import Types
import Utility

type MapPortion = DHL.HashMap Coord Tile -- as it says, contains only a portion of the map.
data LevelMap   = LM Tileset MapSize MapPortion deriving (Show,Eq,Read) -- contains the whole map.
data Tileset = Cave | Temple deriving (Show,Eq,Read)
type MapSize = V2 Int

type Tile = Int
 

data PlayerTurnState =  Start
                      | Move [Direction] Int                  -- directions moved ; maximum number of steps
                      | ChooseTarg (Target Prepared) [Coord] Coord Int -- the target model; the cases already targeted: the pointer; the index of the ability currently targeted (to go back one stage/ confirm the ability)
                      | ChooseAbility Int Int                 -- which ability ? ,max index of abilities
                      | Final AAction

instance Show PlayerTurnState where
  show Start                      = "Start"
  show (Move n m)                 = "Move : " ++ show n ++ " planned, " ++ show m ++ " max"
  show (Final _)                  = "Final"
  show (ChooseTarg tg crds _ n )  = "Choosing Targets with "  ++ show tg ++ " , currently chosen : " ++ show crds ++ " for ability" ++ show n
  show (ChooseAbility n _)        = "Choosing Ability N°"     ++ show n


--BaseTurn => To be done : pass time until at least one actor is ready
--ActorsReady (actors who are ready) => To be done : plan the actions of the actors that are not the player's
--PlayerReady (player's actors who are ready) (actions of the NPC actors) (state of the player turn) => To be done : let the player plan the actions of his actors
--PlansDone   (actions of all actors) => To be done : process the actions of the actors and apply them to the world
--TurnDone (new world state) => To be done : go to the new world state and go back to BaseTurn

data TurnStage  = BlockTurn 
                | BaseTurn 
                | ActorsReady [ActorId] 
                | PlayerReady [ActorId] [AAction] PlayerTurnState 
                | PlansDone [AAction] 
                | TurnDone

data AActionType =  ABAction (Ability Finished)
                  | MAction [Direction] 
                  | DAction (ActorId -> GameLevel -> IO GameLevel) 
                  | NAction 
                  | AlreadyDone Float

data AAction = ACT AActionType ActorId

type PlannedAction = (AAction,Float)

instance Show AActionType where
  show (ABAction ab)   = "ABAction :" ++ show ab
  show (MAction mlist) = "MAction : " ++ show mlist
  show (DAction _)     = "DAction"
  show NAction         = "No Action"
  show (AlreadyDone _) = "Already Done"

instance Show AAction where
  show (ACT a aid) = show a ++" "++ show aid 



isActive :: AAction -> Bool
isActive (ACT x aid) = case x of
  NAction       -> False
  AlreadyDone _ -> False
  _             -> True


instance Show TurnStage where
  show (BlockTurn         )   = "BlockTurn"
  show (BaseTurn          )   = "BaseTurn"
  show (ActorsReady x     )   = "ActorsReady" ++ " "  ++ (concat $ map show x)
  show (PlayerReady x y z )   = "PlayerReady" ++ " "  ++ (concat $ map show x) ++  " "  ++ (show $ length y) ++ (show z)
  show (PlansDone _       )   = "PlansDone"
  show (TurnDone          )   = "TurnDone"   

data GameLevel = GameLevel {
  _time           :: Float,
  _turnStage      :: TurnStage,
  _lvlmap         :: LevelMap,
  _actors         :: DHL.HashMap ActorId Actor,
  _seenMap        :: MapPortion,
  _oldSeenMap     :: MapPortion,
  _lname          :: String,
  _djikstra       :: MapPortion,
  _actionqueue    :: [PlannedAction]
} 

makeLenses ''GameLevel

findActorPerPosition :: Coord -> GameLevel -> Maybe (ActorId,Actor)
findActorPerPosition crd gL = 
  let filmap = toList $ DHL.filter ((==crd).(^.acoord)) (gL^.actors)  in
    case filmap of
      []      -> Nothing
      (x:xs)  -> Just  x

    
countTileOccurences :: Tile -> GameLevel -> Int
countTileOccurences tile gL = count' tile (gL^.lvlmap)

count' ::  Tile -> LevelMap -> Int
count' tl (LM _ _ amap) = length.filter (tl==) $ getSeconds $ DHL.toList amap

getNthTileCoord :: Int -> Tile -> GameLevel ->  Coord
getNthTileCoord n tl gL = V2 x y
  where
    LM _ _ amap = gL^.lvlmap
    tiles = filter ((tl==).getSecond) $ DHL.toList amap
    V2 x y = getFirst $ tiles !! (n-1)

emptyCase :: Coord -> GameLevel -> Bool
emptyCase coord gL = not $ coord `elem` actorcoords
  where
    actorcoords = [(^.acoord) act | act <- DHL.elems (gL^.actors)]

instance Timeable (AAction,Float) where
  getTime (a,t) = t
  passTime t2 (a,t) = traceShowId $ (a,t-t2)
  passTimeIO t2 (a,t) = return $ passTime t2 (a,t)  
    
instance Timeable GameLevel where
  getTime gL = gL^.time
  passTime t gL =  ngL
    where
      ngL = gL  & actors  %~ DHL.map (passActorTime t)
                  & time    %~ (+t) 
  passTimeIO t gL = do
    
    let queue = gL^.actionqueue
    let (actionsToTrigger,newQueue) =  partition ( (<=0).snd.(passTime t) ) queue
    putStrLn $ "Current time is "  ++ (show $ getTime gL)
    putStrLn $ "Preparing to pass " ++ (show t) ++ " time"

    ngL <- procActions gL actionsToTrigger
    let ngL2 = ngL & actionqueue .~ newQueue -- not here
    
    return $ passTime t ngL2

procAction :: GameLevel -> (AAction,Float) -> IO GameLevel
procAction gL ((ACT action aid),time) = do
  donegL <- case action of  MAction mlist   -> trace "actor moving" $ pure $ gL & actors %~ DHL.adjust (moveActor' (map getMv mlist) ) aid
                            ABAction ab     -> trace "ability executing" $ executeAbility ab aid gL                                                                     
                            DAction f       -> f aid gL
                            _               -> pure  gL
  return donegL


procActions :: GameLevel -> [(AAction, Float)] -> IO GameLevel
procActions gL [] = return gL
procActions gL (x:xs) = do
  ngL <- procAction gL x
  procActions ngL xs


executeAbility :: Ability Finished -> ActorId -> GameLevel -> IO GameLevel
executeAbility ab aid gL =  maybeEffect
  where
    
    perp = (gL^.actors) ! aid
    maybeEffect = case (ab^.abtarg, _abeffect ab) of
        (CONFIRM (SC crd), OtherModif frm stats af) -> let  isAct = findActorPerPosition crd gL in
          case isAct of 
            Just (vid,vict)     -> trace ("there is an actor at " ++ show crd ++ ", executing ability") $ procEffect (_abeffect ab) (aid,perp) (vid,vict) gL
            _                   -> trace ("there is no actor at " ++ show crd) $ return gL
        _                                           -> return gL

procEffect :: Effect -> (ActorId,Actor) -> (ActorId,Actor) -> GameLevel -> IO GameLevel
procEffect (OtherModif formula [stat] applyfunc) (aid1,a1) (aid2,a2) gL = do 
  let thestat = (gSt stat a2)
  putStrLn $ "current stat value : "  ++  show thestat
  putStrLn $ "formula result : " ++ show (eval a1 a2 formula)
  putStrLn $ "new stat value : "      ++  show (applyfunc thestat (eval a1 a2 formula))
  return $ gL & actors %~  DHL.adjust (\x -> x & (setSt stat) .~ applyfunc thestat (eval  a1 a2 formula)) aid2
procEffect _ _ _ gL = return gL

