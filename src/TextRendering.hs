module TextRendering where

import Types
import Utility

import Debug.Trace
import System.IO.Unsafe
import Codec.Picture(PixelRGBA8( .. ))
import Graphics.Rasterific
import Graphics.Rasterific.Texture
import Graphics.Gloss.Juicy
import Graphics.Gloss
import GHC.Word
import Graphics.Text.TrueType( loadFontFile )

tRed,tBlack,tClear,tWhite :: (Word8,Word8,Word8,Word8)
tRed    = (255,0,0,255)
tBlack  = (0,0,0,255)
tClear  = (0,0,0,0)
tWhite  = (255,255,255,255)

mainFont = unsafePerformIO $! loadFontFile fontpath
  where
    fontpath = "Fonts/IMFeGPrm28P.ttf"


    -- I struggled with this for a while and I don't even remember why and how most of the values for the fine tuning are selected - but it works. 
displayWord :: String -> Float -> (Word8,Word8,Word8,Word8) -> (Word8,Word8,Word8,Word8) -> Picture
displayWord text size bColor tColor = case mainFont of  
  Left  err   -> trace err blank
  Right font  -> translate (0.4 *(fromIntegral $ adjustX)) (0.4*(fromIntegral adjustY)) $ finalImage'
    where
      size' = truncate $ size*0.8
      tlen  =  length text
      finalImage =  fromImageRGBA8 $ renderDrawing (size'*tlen) (size'*3) (PixelRGBA8 r g b a). withTexture (uniformTexture $ PixelRGBA8 r' g' b' a') $ printTextAt font (PointSize $ (size)) (V2 0 (size*2)) text
      Bitmap adjustX adjustY idata _ = finalImage
      finalImage' = Bitmap adjustX adjustY idata False -- needed to avoid memory leak
      (r,g,b,a) = bColor
      (r',g',b',a') = tColor
