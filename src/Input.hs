{-# LANGUAGE TemplateHaskell, GADTs, ExistentialQuantification #-}
module Input where

import qualified Data.HashMap.Strict as DHL
import Linear.V2
import Linear.V3
import Control.Lens
import Control.Monad
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Control.Concurrent

import Debug.Trace

import Console
import Actors
import Game
import GameLogic
import Level
import Sound
import Types

handleInput :: Event -> Game -> IO Game
handleInput (EventResize (x,y)) game = return $ game & config %~ resize newsize
    where
        newsize = V2 (fromIntegral x) (fromIntegral y)
handleInput event game = case game^.scene of
    AMenu m     -> handleMenu event game
    Level g _   -> case g^.turnStage of
      PlayerReady _ _ ptst -> handleLevel event game >>= handleTurn event ptst
      _               -> handleLevel event game
    _           -> return game
    

handleMenu  :: Event -> Game -> IO Game
handleMenu (EventKey (SpecialKey x ) Down _ _) game = newgame
    where
        AMenu (cmenu,cpos) = game^.scene
        Just (BT lb pic action act) = DHL.lookup cpos cmenu

        whatAction :: Game -> IO Game
        whatAction game = case action of
            IOGlobalAction f  -> f game
            GlobalAction f    -> return $ f game
            MenuAction f      -> return $ game & scene %~ updateMenu f
            NoAction          -> return $ game

        newgame = case x of
            KeyEnter    -> whatAction game
            KeyDown     -> return $ go 2 game
            KeyUp       -> return $ go 8 game
            KeyLeft     -> return $ go 4 game
            KeyRight    -> return $ go 6 game
            KeyEsc      -> endGame game
            _           -> return game

handleMenu _ game = return game

handleLevel :: Event -> Game -> IO Game
handleLevel (EventKey (x ) Down mod _) game
  | cameraKey     = return newCamGame
  | specialKey    = specialGame
  | commandKey    = commHandle
  | otherwise     = return game
  where
    cameraKey       = shift mod == Down && x `elem` [SpecialKey KeyUp,SpecialKey KeyDown,SpecialKey KeyLeft,SpecialKey KeyRight] || x `elem` [Char 'P',Char 'M']
    specialKey      = shift mod == Down && x `elem` [SpecialKey KeySpace,SpecialKey KeyEnter, SpecialKey KeyEsc]
    commandKey      = x `elem` [Char 'a', Char 'b', Char 'c', Char 't', Char 'i', Char 's']
    newCamGame       = game & cameraV %~ (+nv)
    nv = case x of
      SpecialKey KeyDown    -> V3 0 2 0
      SpecialKey KeyUp      -> V3 0 (-2) 0
      SpecialKey KeyRight   -> V3 (-2) 0 0
      SpecialKey KeyLeft    -> V3 2 0 0
      Char 'P'              -> V3 0 0 (-0.05)
      Char 'M'              -> V3 0 0 0.05
      _                     -> V3 0 0 0
    specialGame = case x of
      SpecialKey KeySpace   -> return $ centerCamera game
      SpecialKey KeyEnter   -> return $ resetCamera  game
      SpecialKey KeyEsc     -> endGame game
    commHandle  = case x of
      Char 'w'  -> return $ game & scene %~ blockTurn
      Char 'x'  -> putStrLn (show $ getTurnStage game) >> return game
      Char 'c'  -> do  
                      putStrLn "Starting turn.."
                      return $ game & scene %~ resetTurn
      Char 'v'  -> do 
                      pushConsole (LOG white $ "console test , time : " ++ show(getTime game) )
                      return $ game 
      Char 'b'  -> do -- print actor info
                      let acts = DHL.elems $ getActors game
                      forM_ acts $ \act -> forM_ (actorInfo act ) putStrLn
                      return game
      _         -> return game

handleLevel (EventKey x Up mod _) game
  | cameraKey   = return newCamGame
  | stopCamera  = return noCamGame
  | otherwise   = return game
  where
    cameraKey     = ( shift mod == Down && x `elem` [SpecialKey KeyUp,SpecialKey KeyDown,SpecialKey KeyLeft,SpecialKey KeyRight]) || x `elem` [Char 'P',Char 'M']
    newCamGame    = game & cameraV %~ (+nv)
    nv = case x of
      SpecialKey KeyDown    -> V3 0 (-2) 0
      SpecialKey KeyUp      -> V3 0 2 0
      SpecialKey KeyRight   -> V3 2 0 0
      SpecialKey KeyLeft    -> V3 (-2) 0 0
      Char '+'              -> V3 0 0 (0.05)
      Char '-'              -> V3 0 0 (-0.05)
      _                     -> V3 0 0 0
      
    stopCamera = x `elem` [SpecialKey KeyShiftL, SpecialKey KeyShiftR]
    noCamGame = game & cameraV .~ V3 0 0 0  

handleLevel _ game = return game

handleTurn :: Event -> PlayerTurnState -> Game -> IO Game
handleTurn (EventKey x Down mod _) Start game = case (x,shift mod) of 
  (Char c, Up) -> case c of 
    'e' -> return $ game & scene %~ onTurn' confirmPlayerTurn
    'z' -> return $ game & scene %~ onTurn goBack
    'd' -> return $ game & scene %~ onTurn (enterMove game)
    'a' -> return $ game & scene %~ onTurn (enterAbility game)
    _   -> return game
  (SpecialKey k,Up) -> case k of
    -- KeySpace  -> return $ game & scene %~ onTurn advanceTurn
    _         -> return $ game
  _       -> return game
handleTurn (EventKey x Down mod _) (Move mlist _) game = case (x,shift mod) of 
  (SpecialKey y,Up)   -> case y of
    KeyDown     -> return $ game & scene %~ onTurn (goPlayer 2)
    KeyUp       -> return $ game & scene %~ onTurn (goPlayer 8)
    KeyLeft     -> return $ game & scene %~ onTurn (goPlayer 4)
    KeyRight    -> return $ game & scene %~ onTurn (goPlayer 6)
    KeySpace    -> return $ game & scene %~ onTurn' confirmPlayerTurn 
    _           -> return game
  (Char c, Up)        -> case c of 
    'z'           -> return $ game & scene %~ onTurn goBack
    _             -> return game
  _                   -> return game

handleTurn (EventKey x Down mod _) (ChooseAbility which _) game = case (x,shift mod) of
  (SpecialKey k, Up)  -> case k of
    KeyLeft     -> return $ game & scene %~ onTurn (goAbility 4)
    KeyRight    -> return $ game & scene %~ onTurn (goAbility 6)
    KeySpace    -> return $ game & scene %~ onTurn' confirmPlayerTurn 
    _           -> return game
  (Char c, Up)        -> case c of 
    'z'           -> return $ game & scene %~ onTurn goBack
    _             -> return game
  _                   -> return game
  
handleTurn (EventKey x Down mod _) (ChooseTarg tg _ _ _) game = case (x,shift mod) of
  (SpecialKey y,Up)   -> let in case y of
    KeyDown     -> return $ game & scene %~ onTurn (goTarget' 2)
    KeyUp       -> return $ game & scene %~ onTurn (goTarget' 8)
    KeyLeft     -> return $ game & scene %~ onTurn (goTarget' 4)
    KeyRight    -> return $ game & scene %~ onTurn (goTarget' 6)
    KeySpace    -> return $ game & scene %~ onTurn' confirmPlayerTurn 
    _           -> return game
  (Char c, Up)        -> case c of 
    'z'           -> return $ game & scene %~ onTurn goBack
    'e'           -> return $ game & scene %~ onTurn addTarget
    _             -> return game
  _                   -> return game
handleTurn _ _ game = return game 

