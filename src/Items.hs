 {-# LANGUAGE TemplateHaskell #-}
module Items where

import Data.Map
import Control.Lens
import Linear.V2


data ArmorSlot = Head | Upper | Lower | Hands Int | Shoes | Extra deriving (Show,Ord,Eq,Read)

data Armor = Armor {
    _slots  :: Map ArmorSlot Bool,
    _arname :: String
}deriving (Show,Eq,Read)

data Weapon = Weapon {
    _wpname :: String
}deriving (Show,Eq,Read)


data Item = ARM Armor | WPN Weapon deriving (Show,Eq,Read)



makeLenses ''Armor
makeLenses ''Weapon


