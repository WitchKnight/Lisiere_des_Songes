module Utility where

import Control.Monad
import Data.Hashable
import qualified Data.HashMap.Strict as DHL
import Linear.V2
import Linear.V3

import Types

getSeconds:: [(a,b)]-> [b]
getSeconds [] = []
getSeconds ((a,b):xs) = b:getSeconds xs

getSecond :: (a,b) -> b
getSecond (a,b) = b

getFirsts :: [(a,b)]-> [a]
getFirsts [] = []
getFirsts ((a,b):xs) = a:getFirsts xs

getFirst :: (a,b) -> a
getFirst (a,b) = a

cFLOOR :: Int
cFLOOR = 10

addV2 :: Num a => V2 a -> V3 a -> V3 a
addV2 ( V2 a b) (V3 x y z) = V3 (a+x) (b+y) z

setV2 :: Num a => V2 a -> V3 a -> V3 a
setV2 ( V2 a b) (V3 x y z) = V3 a b z


stick :: a -> b -> (a,b)
stick a b = (a,b)

roundN :: Int -> Float -> Float
roundN n f = (fromInteger $ round $ f * (10^n)) / (10.0^^n)

removeIndex :: Int -> [a] -> [a]
removeIndex n list = (fst $ splitAt n list) ++ (tail $ snd $ splitAt n list)

adjustMultiple :: (Eq k,Hashable k)  => (v->v) -> [k] -> DHL.HashMap k v-> DHL.HashMap k v
adjustMultiple _ [] hm = hm
adjustMultiple f (x:xs) hm = adjustMultiple f xs $ DHL.adjust f x hm


adjustMultiple' :: (Eq k,Hashable k)  => [(k,(v->v))] -> DHL.HashMap k v-> DHL.HashMap k v
adjustMultiple' [] hm = hm
adjustMultiple' ((x,f):xs) hm = adjustMultiple' xs $ DHL.adjust f x hm


insertMultiple :: (Eq k,Hashable k)  => [(k,v)] -> DHL.HashMap k v-> DHL.HashMap k v
insertMultiple [] hm = hm
insertMultiple ((k,v):xs) hm = insertMultiple xs $ DHL.insert k v hm

