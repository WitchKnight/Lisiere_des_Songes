module ActorGen where

import Linear.V2
import Data.Map
import Control.Lens
import qualified Data.HashMap.Strict as DHL
import qualified Data.Map.Lazy as DML

import Actors
import Preload
import Types

startActor = Actor {
    _aname      = "Deonil",
    _acoord     = V2 0 0,
    _astatus    = startStatus,
    _inventory  = fromList [],
    _aclass     =  ActS Hero,
    _abilities  = [punch,kick]
}

startStatus = ActorStatus{
    _spiStats = startStats,
    _actState = startState,
    _afflict  = DML.fromList [],
    _bodyStats= startBody,
    _funcStats= zeroFunc
}



startBody = BodyStats {
  _constitution = 5,
  _strength     = 5,
  _tolerance    = 5,
  _agility      = 5,
  _precision    = 5
 }



startState = Ready
startStats = SpiStats 
  { _power          = 1
  , _passion        = 1
  , _grounding      = 1
  , _awareness      = 1
  , _insight        = 0
  , _resilience     = 1
  , _luck           = 1
  , _despair        = 1
  }


genActorInstance :: ActorClass -> Actor
genActorInstance ac = zeroActor & aclass .~ ac


-- ABILITIES

sglT = PREPARE (BLOCK $ makeCross' 1 ) SINGLEC




punch :: Ability Prepared
punch = Ability {
    _abeffect = punchEffect,
    _abprep = Raw 2,
    _abreco = Raw 0,
    _abname = "punch",
    _abdesc = "a simple punch",
    _abpic  = punchPic,
    _abtarg = sglT
} where
    punchEffect = OtherModif form [HP] (-)
    form = (( (DNF PERP $ gSt POW) !*!  (Raw 2)) !+! ((DNF PERP $ gSt STR) !*! (Raw 2))) !-! (DNF TARG $ gSt CON)

kick :: Ability Prepared
kick = Ability {
    _abeffect = punchEffect,
    _abprep = Raw 1,
    _abreco = Raw 0,
    _abname = "kick",
    _abdesc = "a powerful kick",
    _abpic  = kickPic,
    _abtarg = sglT
} where
    punchEffect = OtherModif form [HP] (-) 
    form =  Min (Add (DNF PERP $ gSt POW) (DNF PERP $ gSt STR)) (DNF TARG $ gSt CON)