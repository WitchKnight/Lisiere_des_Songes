 {-# LANGUAGE TemplateHaskell #-}
module LevelGen where

import qualified Data.HashSet as DH
import qualified Data.HashMap.Strict as DHL
import Control.Lens
import Linear.V2
import Debug.Trace

import Level
import Actors
import ActorGen
import Random
import Types
import Utility

data RoomType = SQ SquareRoom | CR CircularRoom | FL FormlessRoom deriving (Show,Eq)

data SquareRoom = SquareRoom { 
    _startPoint :: V2 Int,
    _endPoint   :: V2 Int
    }deriving (Show,Eq)

bsqr = SquareRoom { 
    _startPoint = V2 0 0,
    _endPoint   = V2 0 0
}
data CircularRoom = CircularRoom {
    _center     :: V2 Int,
    _radius     :: Int
}deriving (Show,Eq)

type FormlessRoom = MapPortion


makeLenses ''SquareRoom
makeLenses ''CircularRoom


class MRoom a where
    rwidth      :: a -> Int
    rheight     :: a -> Int
    rsize       :: a -> (Int,Int)
    rsize a  = (rwidth a, rheight a)
    makeRoom    :: V2 Int -> V2 Int -> a


instance MRoom SquareRoom where
    rwidth sqr = thewidth
        where
            V2 px1 py1   = sqr^.startPoint
            V2 px2 py2   = sqr^.endPoint
            thewidth      = abs $ px1-px2
    rheight sqr = theheight
        where
            V2 px1 py1    = sqr^.startPoint
            V2 px2 py2    = sqr^.endPoint
            theheight     = abs $ py1-py2
    makeRoom p1 p2 = bsqr   & startPoint .~ p1
                            & endPoint   .~ p2

    
 -- toplevel constants

zeroLevel = GameLevel {
  _time           = 0,
  _turnStage      = BlockTurn,
  _actors         = DHL.fromList [(AID (SL 0 1) 0, startActor)],
  _seenMap        = DHL.fromList [],
  _oldSeenMap     = DHL.fromList [],
  _lname          = "",
  _lvlmap         = zeroMap,
  _djikstra       = DHL.fromList [],
  _actionqueue    = []
}

zeroMap = LM Temple (V2 0 0 ) (DHL.fromList[])


startingLevel = zeroLevel     & actors    .~ DHL.fromList [(AID (SL 0 1) 0, startActor)]
                              & lname     .~ "Starting Room"
                              & lvlmap    .~ makeMap 20 20 Temple preMap
-- starting level

preMap = [0,0,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,0,0,0,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,0,1,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,1,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,0,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,0,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,1,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,1,0,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,0,0,0,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,0,0]


--

makeMap :: Int -> Int -> Tileset -> [Int] -> LevelMap
makeMap xsize ysize tls rawdata = LM tls (V2 xsize ysize) (makeMap' (V2 xsize ysize) (V2 0 0) rawdata)


makeMap' ::  V2 Int -> V2 Int -> [Int] -> MapPortion
makeMap' (V2 mpx mpy) _ [] = DHL.empty
makeMap' (V2 mpx mpy) (V2 x y ) (n:ns)
      | x == (mpx-1) = if y == (mpy-1) then DHL.insert (V2 x y) n DHL.empty else DHL.insert (V2 x y) n (makeMap' (V2 mpx mpy) (V2 0 (y+1)) ns)
      | otherwise = DHL.insert (V2 x y) n (makeMap' (V2 mpx mpy) (V2 (x+1) y) ns)



placeActorNthOf :: Int -> Tile -> ActorId -> GameLevel -> GameLevel
placeActorNthOf n tile whom gL = nGL
    where
        newCoord = getNthTileCoord n tile gL
        nGL = gL    & actors %~ placeSingleActor whom newCoord 

addActorNthOf :: Int -> Tile -> ActorId -> Actor -> GameLevel -> GameLevel
addActorNthOf n tile label whom  gL = nGL
    where
        actor = whom & acoord .~ newCoord
        newCoord = getNthTileCoord n tile gL
        nGL = gL    & actors %~ DHL.insert label actor


addActors :: [(ActorId,Actor)] -> GameLevel -> GameLevel
addActors acts gL = gL & actors %~ DHL.union (DHL.fromList acts)
