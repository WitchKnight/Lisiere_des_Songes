module Sound where

import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad ( when, unless )
import Data.List ( intersperse )
import Sound.ALUT
import System.Exit ( exitFailure, exitSuccess )
import System.IO ( hPutStrLn, stderr )
import System.IO.Unsafe

data Music = MainTheme | TempleTheme deriving (Show,Eq,Ord,Read)
data SoundEffect = Confirm | Cancel | Step deriving(Eq,Ord,Read,Show)

data SoundState = SndSt Bool Music deriving(Eq,Ord)



-- I'm sorry dmwit I have failed you - but refactoring my whole code just to pass around this little thing is really the last thing I want to do
musicAllow  = unsafePerformIO $ newEmptyMVar
{-# NOINLINE musicAllow #-} 



soundfolder = "Sound/Fx/"
musicfolder = "Sound/Music/"

mainTheme :: IO Buffer
mainTheme   = createBuffer (File $ musicfolder++"Courage.wav")

templeTheme :: IO Buffer
templeTheme = createBuffer (File $ musicfolder++"Not_So_Subtle.wav")

playMusic :: Music -> IO ()
playMusic  m = do
  -- Create an AL buffer from the given sound file.
  buf <- case m of
    MainTheme   ->  mainTheme
    TempleTheme ->  templeTheme
    _           ->  mainTheme
  -- buf <- mainTheme
  
  -- Generate a single source, attach the buffer to it and start playing.
  source <- genObjectName
  
  buffer source $= Just buf
  play [source]

  -- Normally nothing should go wrong above, but one never knows...
  errs <- get alErrors
  unless (null errs) $ do
    hPutStrLn stderr (concat (intersperse "," [ d | ALError _ d <- errs ]))
    exitFailure

  -- Check every 0.1 seconds if the sound is still playing.
  let waitWhilePlaying = do
        cont <- tryReadMVar musicAllow
        threadDelay 100
        state <- get (sourceState source)
        when (state==Playing && cont /= Nothing) $ do
          waitWhilePlaying
        when (state==Stopped && cont /= Nothing) $ do
          playMusic m
        unless (cont == Just()) $ stop [source]
  waitWhilePlaying


playSound :: SoundEffect -> IO ()
playSound sfx =  do
  -- Create an AL buffer from the given sound file.
  buf <- createBuffer (File $ soundfolder++show sfx++".wav")

  -- Generate a single source, attach the buffer to it and start playing.
  source <- genObjectName
  buffer source $= Just buf
  play [source]

  -- Normally nothing should go wrong above, but one never knows...
  errs <- get alErrors
  unless (null errs) $ do
    hPutStrLn stderr (concat (intersperse "," [ d | ALError _ d <- errs ]))
    exitFailure

  -- Check every 0.1 seconds if the sound is still playing.
  let waitWhilePlaying = do
        threadDelay 100
        state <- get (sourceState source)
        when (state == Playing) $
            waitWhilePlaying
  waitWhilePlaying

-- version to block the whole game when playing the sound.
playSoundW :: SoundEffect -> IO ()
playSoundW sfx =  do
  -- Create an AL buffer from the given sound file.
  buf <- createBuffer (File $ soundfolder++show sfx++".wav")
  -- Generate a single source, attach the buffer to it and start playing.
  source <- genObjectName
  buffer source $= Just buf
  play [source]
  -- Normally nothing should go wrong above, but one never knows...
  errs <- get alErrors
  unless (null errs) $ do
    hPutStrLn stderr (concat (intersperse "," [ d | ALError _ d <- errs ]))
    exitFailure

  -- Check every 0.1 seconds if the sound is still playing.
  let waitWhilePlaying = do
        sleep 0.1
        state <- get (sourceState source)
        when (state == Playing) $
            waitWhilePlaying
  waitWhilePlaying