module Preload where
import Graphics.Gloss
import Graphics.Gloss.Game
import Debug.Trace
import Data.Map
import Data.Maybe


import Types
import Level
import Actors

tilefolder  = "Images/Tiles/"
actorfolder = "Images/Actors/"
iconfolder  = "Images/Icons/"
menufolder  = "Images/Menus/"

preloadedSentientActorSprites :: Map Sentient Picture
preloadedSentientActorSprites =  fromList [ 
                            (Fencer, png (actorfolder ++ "Fighter.png")),
                            (Hero, png (actorfolder ++ "Hero.png")),
                            (Archer, png (actorfolder ++ "Archer.png"))]

preloadedImmobileSprites  :: Map Immobile Picture
preloadedImmobileSprites = fromList [
                        (Statue 0, png (actorfolder ++ "BlueStatue.png")),
                        (Statue 1, png (actorfolder ++ "GuardStatue.png")),
                        (Statue 2, png (actorfolder ++ "GuardStatue.png"))]



fetchActorPic :: ActorClass -> Picture
fetchActorPic aclass  =  case aclass of
  ActS x      -> fromMaybe blank $ Data.Map.lookup x preloadedSentientActorSprites

  ActI x  -> fromMaybe blank $ Data.Map.lookup x preloadedImmobileSprites




getTile :: Tileset -> Int ->  Picture
getTile whatSet x =  genericTiles !! x


genericTiles :: [Picture]
genericTiles =             [ png (tilefolder ++ "wall1_cave.png") 
                           , png (tilefolder ++ "wall2_cave.png") 
                           , png (tilefolder ++ "wall3_cave.png") 
                           , png (tilefolder ++ "wallCH_cave.png")
                           , png (tilefolder ++ "wallCV_cave.png")
                           , png (tilefolder ++ "doorOH.png")     
                           , png (tilefolder ++ "doorOV.png")     
                           , png (tilefolder ++ "doorCV.png")     
                           , png (tilefolder ++ "doorCH.png")     
                           , png (tilefolder ++ "floor.png")
                           , blank]                    

cursorPic   = png $ iconfolder ++ "/Indicators/cursor.png"

consoleBG   = png $ menufolder ++ "consoleBG.png"

selectorPic = png $ iconfolder ++ "/Indicators/selector.png"
rselectorPic = png $ iconfolder ++ "/Indicators/rselector.png"

dotPic      = png $ iconfolder ++ "/Indicators/seldot.png"

arrow8    = png $ iconfolder ++ "/Indicators/to8.png"
arrow4    = png $ iconfolder ++ "/Indicators/to4.png"
arrow6    = png $ iconfolder ++ "/Indicators/to6.png"
arrow2    = png $ iconfolder ++ "/Indicators/to2.png"

getArr :: Int -> Picture
getArr 2 = arrow2
getArr 4 = arrow4
getArr 6 = arrow6
getArr 8 = arrow8


corner7   = png $ iconfolder ++ "/Indicators/c7.png"
corner9   = png $ iconfolder ++ "/Indicators/c9.png"
corner3   = png $ iconfolder ++ "/Indicators/c3.png"
corner1   = png $ iconfolder ++ "/Indicators/c1.png"

horiz     = png $ iconfolder ++ "/Indicators/46.png"
vertc     = png $ iconfolder ++ "/Indicators/28.png"

uTurn     = png $ iconfolder ++ "/Indicators/uturn.png"

getLink :: Int -> Int -> Picture
getLink 2 2 = vertc
getLink 8 8 = vertc
getLink 4 4 = horiz
getLink 6 6 = horiz
getLink 4 8 = corner1
getLink 4 2 = corner7
getLink 2 4 = corner3
getLink 2 6 = corner1
getLink 6 2 = corner9
getLink 6 8 = corner3
getLink 8 4 = corner9
getLink 8 6 = corner7
getLink _ _ = uTurn

getLink' x y = getLink y x


-- ability templates

punchPic = png $ iconfolder ++ "/Skills/Punch10000.png"
kickPic = png $ iconfolder ++ "/Skills/Punch10000.png"
