 {-# LANGUAGE TemplateHaskell, GADTs,TypeSynonymInstances, FlexibleInstances #-}
module Display where


import Control.Lens
import Debug.Trace
import Graphics.Gloss
import Linear.V2
import Linear.V3
import qualified Data.HashMap.Strict as DHL
import Data.HashMap.Strict((!))
import qualified Data.RingBuffer as RB

import Console
import Utility
import Game
import Level
import TextRendering
import Actors
import Preload
import Types

data FixElem  = Movement [Direction] Coord Int      -- current movement; 
              | TargetChooser [Coord] [Coord] Coord -- possible coords; chosen cases, Current Pointer 

data SupElem  = AbilityPicker Int [Picture] String String
              





supElems :: Game -> [SupElem]
supElems g = abElems
  where
    abElems = case (getTurnStage g) of 
        PlayerReady (currentactor:rest) _ (ChooseAbility which _) -> [AbilityPicker which abpics name desc]
            where
                abilities'   = getPropFromId (getActors g) (^.abilities) currentactor
                abpics      = map getPic abilities'
                name        = (abilities' !! which) ^.abname
                desc        = (abilities' !! which) ^.abdesc

        _                            -> []

fixElems :: Game -> [FixElem]
fixElems g = moveElems ++ targElems
  where
    moveElems = case (getTurnStage g) of
        PlayerReady (currentactor:rest) _ (Move mlist max) -> [Movement mlist crd max]
            where 
                crd         = getPropFromId (getActors g) (^.acoord) currentactor
        _                            -> []
    targElems = case (getTurnStage g) of
        PlayerReady (currentactor:rest) _ (ChooseTarg tp chosen pnt abn) ->  [TargetChooser pcrds  (map (+plcrd) chosen) (pnt+plcrd)]
            where
              pl    = (getActors g ! currentactor)
              abilities' = pl^.abilities
              abpics    = map getPic abilities'
              plcrd     = pl^.acoord
              abpic     = abpics !! abn
              pcrds     = case tp of 
                PREPARE (BLOCK crds) _  -> map (+plcrd) crds -- just to test.
                PREPARE (TRANS crds) _  -> map (+plcrd) crds
                _                       -> []
        _                           -> []
renderFinal :: Game -> IO Picture
renderFinal game = do
                      return $ render game c0 c1

class Renderable a where
    render :: a -> V2 Float -> V2 Int -> Picture

class AlterRenderable a where
    render' :: a -> V2 Float -> V2 Int -> Picture


instance Renderable Game where
    render game _ _= pictures [menusRender,levelRender, itemsRender, actorRender,fixedElemsRender, superElemsRender,consoleRender,debugRender]
        where
            V3 cx cy csc = game^.camera
            (csx,csy) = (csc,csc)
            debugRender = blank
            snake = _snake game
            
            --
            menusRender = case game^.scene of
                AMenu m      -> pictures [ render m (V2 h w) (V2 th tw) , render snake (V2 h w) (V2 th tw) ]
                Level g a          -> case a of 
                  NoMenu -> blank
                  LMenu a -> render a (V2 h w) (V2 th tw) 
                SpecialScene    -> blank
            --
            itemsRender = blank

            --
            levelRender =  case game^.scene of
                AMenu m       -> blank
                Level g _          -> scale csx csy $ translate cx cy $ render g (V2 h w) (V2 th tw)
                SpecialScene    -> blank

            --
            actorRender = scale csx csy $ translate cx cy $ renderActors game
            (V2 h w)    = getSize game
            (V2 th tw)  = getTSize game
            

            superElemsRender = case game^.scene of
                AMenu m         -> blank
                Level g NoMenu  -> pictures $ map (\x -> render x (V2 h w) (V2 th tw)) (supElems game)
                SpecialScene    -> blank

            fixedElemsRender = case game^.scene of
              AMenu m         -> blank
              Level g NoMenu  -> scale csx csy $ translate cx cy $ pictures $ map (\x -> render x (V2 h w) (V2 th tw)) (fixElems game)
              SpecialScene    -> blank

            consoleRender = case game^.scene of
              AMenu m         -> blank
              Level g _       -> render gameConsole (V2 h w) (V2 th tw)
              SpecialScene    -> blank


renderActors :: Game -> Picture
renderActors game = case game^.scene of 
    Level g _  -> result
        where
            (V2 h w)    = getSize game
            (V2 th tw)  = getTSize game
            actorlist = DHL.toList $ g^.actors 
            rawactorlist = getSeconds actorlist
            result = pictures [render a (V2 h w) (V2 th tw) | a <- rawactorlist]
    _       -> blank
    
instance Renderable Actor where
    render actor _ (V2 twidth theight) = translate x y $ fetchActorPic (actor^.aclass) 
        where
            V2 x' y' = actor^.acoord
            (x,y) = (fromIntegral $ x'*twidth, fromIntegral $ y'*theight)

instance Renderable GameLevel where
    render level _ (V2 twidth theight) = finalresult
        where
          LM tileset _ actualmap = level^.lvlmap
          cLmap = DHL.toList actualmap
          finalresult =  pictures [translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) (getTile tileset tile) | (V2 xpos ypos,tile) <- cLmap]

instance Renderable Menu where
    render (menu, cursor) (V2 width height) (V2 twidth theight)= finalresult
        where
            finalresult = pictures (buttonpics ++ labelpics ++ cursorpics)
            buttonpics  = [getFinalPic mc bt    | (mc,bt) <- DHL.toList menu]
            labelpics   = [getLabelPic mc bt   | (mc,bt) <- DHL.toList menu]
            cursorpics   = [getCursorPic cursor]
            getFinalPic :: MenuCoord -> Button -> Picture
            getFinalPic (Fixed (V2 xpos ypos)) (BT _ pic _ _) = translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) pic
            getFinalPic (Division x y) (BT _ pic _ _)         = translate (-width/2+ x*width) (-height/2+y*height) pic
            getLabelPic :: MenuCoord -> Button -> Picture
            getLabelPic (Fixed (V2 xpos ypos)) (BT lab _ _ _) = translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) $ render lab (V2 width height) (V2 twidth theight)
            getLabelPic (Division x y) (BT lab _ _ _) = translate (-width/2+x*width) (-height/2+y*height) $ render lab (V2 width height) (V2 twidth theight)
            getCursorPic :: MenuCoord -> Picture
            getCursorPic (Fixed (V2 xpos ypos)) = translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) cursorPic
            getCursorPic (Division x y) =  translate (-width/2+ x*width - 46) (11.5-height/2+y*height) cursorPic

instance AlterRenderable Menu where
    render' (menu, cursor) (V2 width height) (V2 twidth theight)= finalresult
        where
            finalresult = pictures (buttonpics ++ labelpics ++ cursorpics)
            buttonpics  = [getFinalPic mc bt    | (mc,bt) <- DHL.toList menu]
            labelpics   = [getLabelPic mc bt   | (mc,bt) <- DHL.toList menu]
            cursorpics   = [getCursorPic cursor]
            getFinalPic :: MenuCoord -> Button -> Picture
            getFinalPic (Fixed (V2 xpos ypos)) (BT _ pic _ _) = translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) pic
            getFinalPic (Division x y) (BT _ pic _ _)         = translate (-width/2+ x*width) (-height/2+y*height) pic
            getLabelPic :: MenuCoord -> Button -> Picture
            getLabelPic (Fixed (V2 xpos ypos)) (BT lab _ _ _) = translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) $ render lab (V2 width height) (V2 twidth theight)
            getLabelPic (Division x y) (BT lab _ _ _) = translate (-width/2+ x*width) (-height/2+y*height) $ render' lab (V2 width height) (V2 twidth theight)
            getCursorPic :: MenuCoord -> Picture
            getCursorPic (Fixed (V2 xpos ypos)) = translate (fromIntegral $ twidth*xpos) (fromIntegral $ theight*ypos) cursorPic
            getCursorPic (Division x y) =  translate (-width/2+ x*width - 46) (11.5-height/2+y*height) cursorPic




instance Renderable Label where
  render (LB t c (sx,sy)) _ _= color c $ scale (sx*2) (sy*2) $ displayWord t (23*2)  tClear tWhite

instance AlterRenderable Label where
  render' (LB t c (sx,sy)) _ _ = color c $ scale sx sy $ text t

instance Renderable  GameConsole where
  render gC (V2 width height) (V2 twidth theight) = translate (0) (-height/2+120) $ pictures  [ consoleBG', consoleText ] 
    where
      consoleBG'  =  translate 0 0 $ consoleBG
      consoleText =  finalRender $ spreadDisplay $ getConsoleInfo
      getConsoleInfo = [ translate (-350) (-90) $ scale 0.2 0.15 $ color c $ text t |(t,c) <-  map viewL $ flatten gC ]
      spreadDisplay pics = zip pics [ 0.8* (fromIntegral $ theight*n) | n <- [1.. length pics] ]
      finalRender infoPics = pictures [translate 0 y $ p | (p,y) <- infoPics ]

instance Renderable FixElem where
  render (Movement mlist (V2 px py) maxStride) (V2 width height) (V2 twidth theight) = pictures (currentPath ++ possibleMoves ++ extraMsgs)
    where
      currentPath = map (placeMove) moves
      ns = V2 0 0
      moves      = buildMoves mlist (V2 0 0) 5
      V2 fsx fsy  = fst $ last moves
      placeMove (V2 x y, pic) = translate (fromIntegral $ twidth*(px+x-fsx)) (fromIntegral $ twidth*(py+y-fsy)) pic
      buildMoves ::   [Direction]                               -- directions from which to build the display
                      -> Coord                                  -- total  "stack" of moves since the start of the build. Starts at V2 0 0
                      -> Direction                              -- last move made; 5 means we're at the start
                      -> [(Coord,Picture)]                      -- final result : list of pics with their coords, stack, 
      buildMoves [] stack _  = (stack, selectorPic):[]      -- if we finish the list, we know there is only the move selector to display.
      buildMoves (ndir:xs) st oldir = case oldir of 
        5 -> (ns, getArr ndir)              : buildMoves xs (getMv' ndir) ndir  -- at the start
        n -> (st, getLink ndir n )          : buildMoves xs (st+move) ndir where move = getMv' ndir
      possibleMoves = let (V2 mx my) = sum (map getMv mlist) in [translate (fromIntegral $ twidth*x) (fromIntegral $ twidth*y) $ selectorPic | V2 x y <- getMoveArea (V2 (px+mx) (py+my)) (length mlist) maxStride]
      extraMsgs     = [turnWarning] 
      turnWarning   = if (length mlist <= maxStride) then blank else translate (fromIntegral $ twidth*(px-fsx)) (fromIntegral $ twidth*(py-fsy+1 )) $ render (LB ("Turns : " ++ (show $ truncate $ trnB+1)) white (0.12, 0.12)) (V2 width height) (V2 twidth theight)
      trnB          = roundN 2 (fromIntegral $ length mlist)/ (fromIntegral maxStride)

  render (TargetChooser pcrds crds pnt) (V2 width height) (V2 twidth theight) = pictures [ possibleTargs, chosenTargs, pointer]
    where
      acoord pic (V2 x y ) = translate (fromIntegral $ x*twidth) (fromIntegral $ y*twidth) pic
      pointer = acoord dotPic pnt
      chosenTargs   = pictures $ map (acoord rselectorPic) crds
      possibleTargs = pictures $ map (acoord selectorPic) pcrds
        
instance Renderable SupElem where
  render (AbilityPicker which abpics name desc) (V2 width height) (V2 twidth theight) = translate (-width/2 + fromIntegral twidth) (height/2 - (fromIntegral $ 2*theight)) $ scale 2 2 $ pictures [nameLab,descLab,translate 0 (0- fromIntegral theight) $ pictures $ [abilityPics,abChooser]]
    where
      abChooser   = (translate (fromIntegral $ which*twidth) 0 selectorPic)
      abilityPics = pictures [translate (fromIntegral $ twidth*x) 0 p | (p,x) <- spreadPics ] 
      spreadPics  = zip abpics [0..(length abpics)-1]
      nameLab = translate (0) (0) $ render (LB name white (0.18, 0.18)) (V2 width height) (V2 twidth theight)
      descLab = translate (10) (-50) $ render (LB desc white (0.12, 0.12)) (V2 width height) (V2 twidth theight)


instance Renderable SNAKE where
    render (Snake crds length) (V2 width height) (V2 twidth theight) = finalpic
      where
        turnToPic (V2 x y) = translate (fromIntegral $ twidth*x) (fromIntegral $ twidth*y) $ selectorPic
        headToPic (V2 x y) = translate (fromIntegral $ twidth*x) (fromIntegral $ twidth*y) $ rselectorPic
        snakepos    = [ turnToPic x | x <- crds]
        finalpic    = pictures [tailpic,headpic]
        tailpic     = pictures $ tail $ snakepos
        headpic     = headToPic $ head $ crds 