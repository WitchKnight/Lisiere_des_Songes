 {-# LANGUAGE TemplateHaskell, GADTs #-}
module Types where
import Control.Lens
import Graphics.Gloss
import Linear.V2
import Data.Hashable
import Debug.Trace
import qualified Data.HashMap.Strict as DHL

data GameConfig = GC (V2 Float) (V2 Int) deriving (Show,Eq,Read,Ord)

data MenuCoord  = Fixed Coord | Division Float Float deriving (Show,Eq,Read,Ord)

instance Hashable MenuCoord where
    hashWithSalt s (Fixed c) = s + hash c
    hashWithSalt s (Division x y ) = s + hash (x/y)

data SpecialFlag = QUIT | SAVE | LOAD deriving (Show,Eq,Read)

type SpecialFlags = DHL.HashMap SpecialFlag Bool

instance Hashable SpecialFlag where
    hashWithSalt s a = s + hash (show a)

type Time = Float

type Coord = V2 Int
c0  :: V2 Float
c0 = V2 0 0
c1  :: V2 Int
c1 = V2 0 0
type Direction = Int

getMv :: Direction -> Coord
getMv 2 = V2 0 (-1)
getMv 4 = V2 (-1) 0
getMv 6 = V2 1 0
getMv 8 = V2 0 1 
getMv 5 = V2 0 0
getMv _ = getMv 5

getMv' = (V2 0 0-).getMv


data ActorId    = AID Soul Identifier deriving (Show,Eq,Read,Ord)   --effects are applied to actors depending on the soul. the identifier is for the game turns, the individual deaths, etc. Souls are very rarely shared.

createCID :: Int -> ActorId
createCID n  = AID (SL (-1) 0) (-n)

type Identifier = Int
data Soul       = SL Int Float deriving (Show,Eq,Read,Ord)          --soul identifier, soul power. soul power multiplies all the stats by itself but too much soul power attracts attention; and also risks the risk of soul overload if the body can't contain it.

playerSoul = AID (SL 0 (-1)) (-1)

class SpecEq a where
  (<~>):: a -> a -> Bool
    
instance SpecEq Soul where
  (<~>) (SL n f) (SL n2 f2) = n == n2  

instance SpecEq ActorId where
  (<~>) (AID sl _) (AID sl2 _) = sl <~> sl2

instance Hashable ActorId where
  hashWithSalt s (AID (SL soulnumber soulpower) identifier) = s + hash soulnumber + hash soulpower + hash identifier 

newtype Turn = TURN Identifier deriving (Show,Eq,Read,Ord)

class Timeable a where
  getTime   :: a -> Float
  passTime  :: Float -> a -> a
  passTimeIO :: Float -> a -> IO a

data SG
data PT =  PERP | TARG


data Formula a b where
  DNF           :: PT -> (a -> Float) -> Formula a PT
  SNF           :: (a -> Float)  -> Formula a SG
  Add           :: Formula a  b -> Formula a b -> Formula a b 
  Mul           :: Formula a  b -> Formula a b -> Formula a b 
  Div           :: Formula a  b -> Formula a b -> Formula a b
  Pow           :: Formula a  b -> Formula a b -> Formula a b
  Min           :: Formula a  b -> Formula a b -> Formula a b
  Raw           :: Float -> Formula a b
  
-- instance Show (Formula a b) where
--   show (DNF )

(!*!),(!+!),(!-!),(!^!),(!/!) :: Formula a  b -> Formula a b -> Formula a b 
  
(!*!) a b = Mul a b
(!+!) a b = Add a b
(!-!) a b = Min a b
(!^!) a b = Pow a b
(!/!) a b = Div a b


eval :: a -> a -> Formula a b -> Float
eval a1 _  (DNF PERP f) = f a1
eval _ a2  (DNF TARG f) = f a2
eval a1 _  (SNF f)      = f a1
eval _  _  (Raw fl)     = fl
eval a1 a2 (Add n1 n2)  = eval a1 a2 n1 +  eval a1 a2 n2
eval a1 a2 (Mul n1 n2)  = eval a1 a2 n1 *  eval a1 a2 n2
eval a1 a2 (Div n1 n2)  = eval a1 a2 n1 /  eval a1 a2 n2
eval a1 a2 (Pow n1 n2)  = eval a1 a2 n1 ** eval a1 a2 n2
eval a1 a2 (Min n1 n2)  = eval a1 a2 n1 -  eval a1 a2 n2

evalS act frml = eval act act frml

makeCross :: Int -> Coord -> [Coord]
makeCross n (V2 sx sy) = [V2 (sx+x) (sy+y) | x <- [-n..n],y <-[-n..n],(-x-y)<=n,x+y<=n,x-y<=n,y-x <=n]

makeCross' n = makeCross n (V2 0 0)


rev :: (a -> a -> b ) -> a-> a -> b
rev f a1 a2 = f a2 a1  


data SNAKE = Snake [Coord] Int deriving (Eq,Read,Ord,Show)

move :: Coord -> SNAKE ->  SNAKE
move crd (Snake crds length) = Snake (take length $ (crd2:crds)) length
  where
    crd2 = crd + head crds