 {-# LANGUAGE TemplateHaskell, GADTs #-}
module Game where

import Control.Lens
import Control.Concurrent
import Control.Monad
import System.Random
import Data.STRef
import Control.Monad
import Data.List
import Linear.V2
import Linear.V3
import Graphics.Gloss
import Data.HashMap.Strict as DHL
import Debug.Trace
import System.Exit

import Console
import Items
import Actors
import ActorGen
import Level
import LevelGen
import Sound
import Types
import Utility
import Random
--Game types

data Button = BT Label Picture Action Active -- Active designs whether you can select that button by moving with arrow keys

instance Show Button where
  show (BT (LB text _ _) p action active) = "Button : " ++ text ++ actionType ++ isActive
    where
      actionType = case action of
        IOGlobalAction _  -> " action type : IO Game "
        GlobalAction _    -> " action type : Game "
        IOMenuAction _    -> " action type : IO Menu "
        MenuAction _      -> " action type : Menu "
        NoAction          -> " action type : None "
      isActive = case active of
        True  -> "Active element"
        False -> "Passive element"


data Label  = LB String Color (Float,Float)
type Camera = V3 Float
type Active = Bool
data Action = IOGlobalAction (Game -> IO Game) | GlobalAction (Game -> Game) | IOMenuAction (Menu -> IO Menu)| MenuAction (Menu -> Menu) | NoAction

data LevelMenu = LMenu Menu | NoMenu

type Menu = (HashMap MenuCoord Button,  MenuCoord)

--Game types

data GameScene = AMenu Menu | Level GameLevel LevelMenu| SpecialScene 
--

data Game = Game {
  _config     :: GameConfig,
  _player     :: Int, --Soul Number
  _scene      :: GameScene,
  _camera     :: V3 Float,
  _cameraV    :: V3 Float,
  _seed       :: Seed Int,
  _nextLevel  :: GameLevel,
  _spFlags    :: SpecialFlags,
  _debugFlag  :: Bool,
  _soundState :: SoundState,
  _snake      :: SNAKE
} 
makeLenses ''Game

instance Timeable Game where
  getTime g     = case g^.scene of 
    Level gL _  -> getTime gL
    _           -> 0 
  passTime t g  = case g^.scene of 
    Level gL x  -> g & scene .~ Level (passTime t gL) x
    _           -> g
  passTimeIO t g = case g^.scene of 
    Level gL x  -> do
        newL <- passTimeIO t gL 
        return $ g & scene .~ Level newL x
    _           -> pure g
-- initial values; menus; etc

initialState = Game {
  _config     = GC (V2 1024 768) (V2 23 23),
  _player     = 0, --SL 0 _ is always the player.
  _scene      = AMenu mainMenu,
  _camera     = V3 0 0 2,
  _cameraV    = V3 0 0 0,
  _seed       = seed0,
  _spFlags    = DHL.fromList [(QUIT,False),(LOAD,False),(SAVE,False)],
  _nextLevel  = startingLevel,
  _debugFlag  = False,
  _soundState = SndSt False MainTheme,
  _snake      = Snake [(V2 0 0)] 7 
}
-- convenience functions

mPlaying :: Game -> Bool
mPlaying game = answer
  where
    SndSt answer _ = game^.soundState

inLevel :: Game -> Bool
inLevel game = case game^.scene of 
  Level g _ -> True
  _         -> False

inMenu :: Game -> Bool
inMenu game = case game^.scene of 
  AMenu _   -> True
  _         -> False

silentZone :: Game -> Bool
silentZone game = False

getTurnStage :: Game ->TurnStage
getTurnStage game = case game^.scene of
  Level gL _  -> gL^.turnStage
  _           -> BlockTurn

getActors :: Game -> DHL.HashMap ActorId Actor
getActors g = case g^.scene of 
  Level gL _    -> gL^.actors
  _             -> DHL.fromList []

currentActor :: Game -> ActorId
currentActor g = case g^.scene of
  Level gL _      ->  case gL^.turnStage of 
    PlayerReady []     _ _  -> playerSoul
    PlayerReady (x:xs) _ _  -> x
  _               ->  playerSoul


blockTurn :: GameScene -> GameScene
blockTurn (Level gL m) = Level (gL & turnStage .~ BlockTurn) m
blockTurn a = a

resetTurn :: GameScene -> GameScene
resetTurn (Level gL m) = Level (gL & turnStage .~ BaseTurn) m
resetTurn a = a

setTurn :: TurnStage -> GameScene -> GameScene
setTurn tS (Level gL m) = Level (gL & turnStage .~ tS) m

onTurn :: (TurnStage -> TurnStage) -> GameScene -> GameScene
onTurn f (Level gL m) = Level (gL & turnStage %~ f) m

onTurn' :: (GameLevel -> TurnStage -> TurnStage) -> GameScene -> GameScene
onTurn' f (Level gL m) = Level (gL & turnStage %~ f gL) m


-- Menu

menuTitle   = BT (LB "Rogh" white (0.5, 0.5))       blank NoAction False
  
startButton = BT (LB "Start game" white (0.2, 0.2)) blank (IOGlobalAction startGame) True

optButton   = BT (LB "Options" white (0.2, 0.2))    blank NoAction True

quitButton  = BT (LB "Quit" red (0.2,0.2))          blank (IOGlobalAction endGame) True

mainMenu :: Menu
mainMenu = (fromList [(Division 0.4 0.9, menuTitle),(Division 0.1 0.5, startButton), (Division 0.1 0.7, optButton), (Division 0.1 0.3, quitButton)], Division 0.1 0.5)

updateMenu :: (Menu-> Menu) -> GameScene -> GameScene
updateMenu f (AMenu m) = AMenu (f m)
updateMenu f other = other 

-- Options Menu

menuTitle2 = BT (LB "Options" white (0.5, 0.5)) blank NoAction False

-- Level Menu functions (function of the gameState )

statLabel str = BT (LB str white (0.15,0.15)) blank NoAction False

catLabel str = BT (LB str white (0.2,0.2)) blank NoAction False

-- ~ makeStatScreen :: Actor -> HashMap MenuCoord Button
-- ~ makeStatScreen act = 
  -- ~ where
    -- ~ energ = (gFun act)^.energy
    -- ~ conc  = (gFun act)^.concentration
    -- ~ hp    = (gFun act)^.health
    
    -- ~ con   = (gBod act)^.constitution
    -- ~ str   = (gBod act)^.strength
    -- ~ tol   = (gBod act)^.tolerance
    -- ~ agi   = (gBod act)^.agility
    -- ~ pre   = (gBod act)^.precision
    
    -- ~ awar  = (gSpi act)^.awareness
    -- ~ pass  = (gSpi act)^.passion
    -- ~ desp  = (gSpi act)^.despair
    -- ~ lck   = (gSpi act)^.luck


-- ~ levelMenu :: GameLevel -> Menu

getSize game = size
    where
        GC size _= game^.config

getTSize game = tsize
    where
        GC size tsize = game^.config


resize :: V2 Float -> GameConfig -> GameConfig
resize (V2 x y) (GC (V2 x1 y1)  z) = GC (V2 x y) z

activeElement :: Button -> Bool
activeElement (BT _ _ _ active) = active

go :: Direction -> Game -> Game
go direction game = game  & scene %~ go' direction
                          & snake %~ move (getMv direction)
  where
    go' direction (AMenu (m,c)) = AMenu (m,getClosest game direction c m) 

getClosest :: Game -> Direction -> MenuCoord -> HashMap MenuCoord Button -> MenuCoord 
getClosest game direction c m = nc
  where
    activeElements = DHL.filter activeElement m 
    activeOtherElements = Data.List.filter (/=c) $ keys activeElements
    md = minimum [abs $ getDistance game direction c c2 | c2 <- activeOtherElements]
    nc = head' $ [ crd | crd <- activeOtherElements, getDistance game direction c crd == md]
    head' [] = c
    head' (x:xs) = x
getDistance ::Game -> Direction -> MenuCoord -> MenuCoord -> Float
getDistance game direction c1 c2 = d
  where
    V2 gwidth gheight = getSize game
    d = case c1 of 
      Fixed fc1   -> df1 fc1
      Division fd11 fd12  -> dd1 (fd11,fd12)
    df1 x = case c2 of
      Fixed fc2   -> df1f2 x fc2
      Division fd21 fd22  -> df1d2 x (fd21,fd22)
    dd1 x = case c2 of
      Fixed fc2   -> dd1f2 x fc2 
      Division  fd21 fd22  -> dd1d2 x (fd21,fd22)
    df1f2 (V2 x1 y1) (V2 x2 y2) =  case direction of
      8 -> fromIntegral (y2-y1)
      6 -> fromIntegral (x2-x1)
      4 -> fromIntegral (x1-x2)
      2 -> fromIntegral (y1-y2)
    df1d2 (V2 x1 y1) (x2, y2)   =  case direction of
      8 -> (y2*gheight-fromIntegral y1)
      6 -> (x2*gwidth-fromIntegral x1)
      4 -> (fromIntegral x1-x2*gwidth)
      2 -> (fromIntegral y1-y2*gheight)
    dd1f2 (x1,y1) (V2 x2 y2)    =  case direction of
      8 -> (fromIntegral y2-y1*gheight)
      6 -> (fromIntegral x2-x1*gwidth)
      4 -> (x1*gwidth-fromIntegral x2)
      2 -> (y1*gheight-fromIntegral y2)
    dd1d2 (x1,y1) (x2,y2)       =  case direction of
      8 -> (y2-y1)
      6 -> (x2-x1)
      4 -> (x1-x2)
      2 -> (y1-y2)  

-- getClosest' :: Menu -> MenuCoord 
-- getClosest' c m = nc
--   where
--     nc = minimum [getDistance' c c2 | c2 <- keys m]

-- getDistance' :: MenuCoord -> MenuCoord -> Float
-- getDistance' c1 c2 = d
--   where
--     d = case c1 of 
--       Fixed fc1   -> df1 fc1
--       Division fd11 fd12  -> dd1 (fd11,fd12)
--     df1 x = case c2 of
--       Fixed fc2   -> df1f2 x fc2
--       Division fd21 fd22  -> df1d2 x (fd21,fd22)
--     dd1 x = case c2 of
--       Fixed fc2   -> dd1f2 x fc2 
--       Division  fd21 fd22  -> dd1d2 x (fd21,fd22)
--     df1f2 (V2 x1 y1) (V2 x2 y2) =  sqrt $ (x1-x2)^2 + (y1-y2)^2
--     df1d2 (V2 x1 y1) (x2, y2)   =  sqrt $ (x1-x2*gwidth)^2 + (y1-y2*gheight)^2
--     dd1f2 (x1,y1) (V2 x2 y2)    =  sqrt $ (x2-x1*gwidth)^2 + (y2-y1*gheight)^2
--     dd1d2 (x1,y1) (x2,y2)       =  sqrt $ (x2*gwidth-x1*gwidth)^2 + (y2*gheight-y1*gheight)^2

startGame :: Game -> IO Game
startGame game = trace "starting game" newgame 
  where
    loadNextG   = game & scene .~ Level (game^.nextLevel) NoMenu
    plPlG       = placePlayer loadNextG
    plStG       = placeStatues plPlG
    ctCamG      = centerCamera plStG
    SndSt mPlaying musId = game^.soundState
    newgame     = do
      when mPlaying $ takeMVar musicAllow
      forkOS $ playSound Confirm
      return $ ctCamG & soundState .~ SndSt False TempleTheme
  
endGame :: Game -> IO Game
endGame game = trace "ending game" $ do
                  tryTakeMVar musicAllow -- ends music before quitting
                  return $ game & spFlags %~ toggle QUIT

toggle :: SpecialFlag -> SpecialFlags -> SpecialFlags
toggle fl flags = adjust (not) fl flags

placePlayer :: Game -> Game
placePlayer game =  trace ("placing player at the " ++ show randn ++ "nth empty floor tile" ) $
                    game  & scene %~ placePlayer'' randn 
                          & seed  %~ useSeed
    where
        Level gL _  = game ^.scene
        placePlayer'' n (Level l a ) = Level (placePlayer' n l) a
        placePlayer'' n sc = sc
        randn = getIntRange (game^.seed) (1,countTileOccurences cFLOOR gL)

placePlayer' :: Int -> GameLevel -> GameLevel
placePlayer' n = placeActorNthOf n cFLOOR (AID (SL 0 1) 0)

placeStatues :: Game -> Game
placeStatues game = game  & scene %~ placeStatues' randn1
                          & seed  %~ useSeedX (randn2)
    where
      Level gL _  = game ^.scene
      placeStatues' n (Level l a ) = Level (addStatues randn1 l) a
      randn1 = nub $ [ getIntRange (useSeedX n  $ game^.seed) (1, countTileOccurences cFLOOR gL) | n <- [1,randn2]]
      randn2 = getIntRange (game^.seed) (1,40) --max 10 statues in the first level
      
-- this is very ugly
addStatues :: [Int] -> GameLevel -> GameLevel
addStatues positions gL = addActors (zip idlist actlist ) gL
  where
    idlist  = [ AID (SL n 1) (n+1) | n <- [1..(length positions+1)] ]
    actlist = [genActorInstance (ActI $ Statue 1) & acoord .~ getNthTileCoord position cFLOOR gL 
                                                  & aname .~ "Statue" 
                                                  & astatus.actState .~ Inactive | position <- positions , emptyCase (getNthTileCoord position cFLOOR gL)gL]

getPlayerCoord :: Game -> Coord
getPlayerCoord game = case game^.scene of
  Level gL _ -> crd
    where
      actors' = gL^.actors
      plact = getSecond.head $ getBySoul 0 actors'
      crd = plact^.acoord
  _       -> V2 0 0
  
centerCamera :: Game -> Game
centerCamera game = game & camera %~ (setV2 $ V2 (fromIntegral $ (-23)*ax) (fromIntegral $ (-23)* ay))
  where
    V3 _ _ zoom = game^.camera
    V2 ax' ay'  = getPlayerCoord game
    (ax,ay)
      | zoom > 0 = (ax',ay')
      | otherwise = (ax',ay')

resetCamera :: Game -> Game
resetCamera game = game & camera .~ (V3 (fromIntegral $ (-23)*ax) (fromIntegral $ (-23)* ay) 2)
  where
    V2 ax ay  = getPlayerCoord game