 {-# LANGUAGE TemplateHaskell, GADTs,TypeSynonymInstances,FlexibleInstances, FlexibleContexts#-}
module Console where

import qualified Data.Vector as V
import Graphics.Gloss
import Control.Monad
import Control.Lens
import Control.Concurrent (threadDelay)
import Data.IORef
import Debug.Trace
import Pipes
import Pipes.Concurrent
import System.IO.Unsafe
import qualified Data.RingBuffer as RB

import Types

data Log = EmptyLog | LOG Color String deriving (Eq,Show)

class Loggable a where
  turnToLog   :: a -> Log
  mturnToLog  :: [a] -> [Log]
  mturnToLog [] = EmptyLog:[]
  mturnToLog (x:xs) = turnToLog x: (mturnToLog xs)
  -- ~ glog        :: a -> GameConsole -> IO ()
  -- ~ mglog       :: [a] -> IO ()
  
instance Loggable Log where
  turnToLog l = l

instance Loggable String where
  turnToLog s = LOG white s

viewL :: Log -> (String,Color)
viewL (LOG c t) = (t,c)

type GameConsole = RB.RingBuffer V.Vector Log


-- Global variable for the game console.
gameConsole :: GameConsole
{-# NOINLINE gameConsole #-}
gameConsole = unsafePerformIO $ RB.new 8


full:: GameConsole -> IO Bool
full gc = do
  len' <- RB.length gc
  return  $ len' ==  RB.capacity gc
            

pointer :: IORef Int
pointer = unsafePerformIO $ newIORef 0 --necessary to roll the console correctly.

pushPointer :: IO()
pushPointer = do
                pointer' <- readIORef pointer
                len' <- RB.length gameConsole
                if (pointer'<(len'-1)) then do 
                    modifyIORef' pointer (+1) 
                else do 
                    writeIORef pointer 0

pushConsole :: Loggable a => a -> IO()
pushConsole a = do
            isFull <- full gameConsole
            RB.append (turnToLog a) gameConsole
            if isFull then do pushPointer else return ()
      
consTrace :: Loggable a=> a -> b -> b
consTrace a b = unsafePerformIO $ do
                  pushConsole a
                  return b



-- the actual toList function
flatten ring = unsafePerformIO $ do 
  startlist <-  RB.toList ring
  pointer'  <-  readIORef pointer
  let (end,start) = splitAt (length startlist-pointer') startlist 
  return (start ++ end)



