module Random where


data Seed a = SeedSt a Int deriving (Show,Eq,Read)

instance Functor Seed where
  fmap f (SeedSt a age) = SeedSt (f a) (age+1)


fxs1 = 123456
fxs2 = 654321
fxs3 = 147258
fxs4 = 852741

mx = 177177
ax = 12345
bx = 123.45


seed0 :: Seed Int
seed0 = SeedSt fxs4 0




randomize :: (a->a) -> Seed a -> Seed a
randomize f (SeedSt s a) = SeedSt (f s) a 


useSeedG :: (a->a) -> Seed a -> Seed a
useSeedG f sd = fmap f sd

useSeedX ::Int -> Seed Int -> Seed Int
useSeedX n sd
  | n == 0 = sd
  | otherwise = useSeed $ useSeedX (n-1) sd

useSeedGX :: Int -> (a -> a) -> Seed a -> Seed a
useSeedGX n f sd
   | n == 0 = sd
   | otherwise = useSeedG f $ useSeedGX (n-1) f sd

ageFunc :: (Int -> Int) -> Seed a -> Seed a
ageFunc f (SeedSt v age) = SeedSt v (f age)

getAge :: Seed a -> Int
getAge (SeedSt _ age) = age 

getVal  :: Num a => Seed a -> a
getVal (SeedSt val age)     = val 

class ClassicSeed a where
  getInt      :: Seed a -> Int
  getIntRange :: Seed a -> (Int,Int) -> Int
  useSeed     :: Seed a -> Seed a
  rollDice    :: Seed a -> Seed a

instance ClassicSeed Int where
  rollDice (SeedSt val age) = SeedSt  ((val*mx) `mod` 1000000) age
  useSeed (SeedSt val age)  = useSeedG ((`mod` 1000000).(+ax).(* (mx*age))) (SeedSt val age)
  getInt (SeedSt val age) = val+age
  getIntRange (SeedSt val age) (a,b) = result
    where
      (mx, mn) = (max a b, min a b)
      ceil = mx-mn
      result = (val `mod` ceil) + mn

instance ClassicSeed Float where
  rollDice (SeedSt val age) = SeedSt  (fromIntegral $ (truncate val)*mx `mod` 1000000) age
  useSeed (SeedSt val age) = useSeedG ((+bx).(*(fromIntegral $ mx*age))) (SeedSt val age)
  getInt (SeedSt val age) = round val + age
  getIntRange (SeedSt val age) (a,b) = result
    where
      (mx, mn) = (max a b, min a b)
      ceil = mx-mn
      result = ((round val) `mod` ceil) + mn

