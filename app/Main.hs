module Main where

import Actors
import Items
import Game
import GameLogic
import Input
import Display
import Console
import Sound.ALUT

import Control.Concurrent
import Control.Concurrent.MVar
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game


gameName = "Rogh"
gameVersion = "0.1"

windowDimensions = (1024,768)
windowOffset = (50,50)

window = InWindow (gameName ++ gameVersion) windowDimensions windowOffset

background :: Color
background = black

main :: IO ()
main = withProgNameAndArgs runALUT $ \_ _ -> do
    playIO window background 60 initialState renderFinal handleInput updateGame 






