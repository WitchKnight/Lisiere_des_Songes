{nixpkgs ? import <nixpkgs> { }, ghc ? nixpkgs.ghc}:

with nixpkgs;

haskell.lib.buildStackProject {
  name = "Lisiere";
  buildInputs = [ 
    #glfw3
    mesa
    freealut
    freeglut
    openal
    #xorg.xproto
    #xorg.libX11
    #xorg.libX11.dev
    #xorg.libXrandr
    #xorg.libXrandr.dev
    #xorg.libXinerama
    #xorg.libXext
    #xorg.libXcursor
    #xorg.libXi
    #xorg.libXxf86vm
    zlib.dev 
    zlib.out 
    pkgconfig
 ];
  inherit ghc;
}
